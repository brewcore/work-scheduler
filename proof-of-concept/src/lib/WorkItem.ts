/*
This file is part of the Caplan software tool.

Copyright (C) 2021 Tyler Golden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
export default class WorkItem {
  private _estimateHours: number;
  private _projectName: string;
  private _uniqueId: string;

  constructor (projectName: string, uniqueId: string, estimateHours?: number) {
    this._estimateHours = estimateHours ? estimateHours : 0
    this._projectName = projectName
    this._uniqueId = uniqueId
  }

  get EstimateHours() {
    return this._estimateHours
  }
  
  get ProjectName() {
    return this._projectName
  }

  get UniqueId() {
    return this._uniqueId
  }
}