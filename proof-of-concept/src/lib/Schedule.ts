/*
This file is part of the Caplan software tool.

Copyright (C) 2021 Tyler Golden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { DateTime, Duration, Interval } from 'luxon'
import WorkItem from './WorkItem'
import ScheduleEvent from './ScheduleEvent'

export interface ScheduleConfig {
  start: DateTime
  end: DateTime
  workingHoursPerDay: Duration
  nonWorkingDaysOfWeek?: number[] // 1-7 representing Monday-Sunday
  holidays?: DateTime[]
}

export interface AvailabilityForDay {
  capacityHours: number
  date: DateTime
}

export default class Schedule {
  private readonly _scheduleInterval: Interval
  private readonly _events: ScheduleEvent[]
  private readonly _workingHoursPerDay: Duration
  private readonly _nonWorkingDaysOfWeek?: number[]
  private readonly _holidays?: DateTime[]

  constructor (work: WorkItem[], config: ScheduleConfig) {
    this._scheduleInterval = Interval.fromDateTimes(config.start, config.end)
    this._workingHoursPerDay = config.workingHoursPerDay
    if (config.nonWorkingDaysOfWeek) {
      // TODO validate 1-7
      this._nonWorkingDaysOfWeek = config.nonWorkingDaysOfWeek
    }
    if (config.holidays) {
      this._holidays = config.holidays
    }

    const events: ScheduleEvent[] = []

    let currentTime = config.start

    work.forEach(item => {
      let scheduledItemHours = 0
      let scheduledDayHours = 0
      do {
        if (scheduledDayHours < this._workingHoursPerDay.hours) {
          const remainingHrsInDay = this._workingHoursPerDay.hours - scheduledDayHours
          const remainingEstimateHrs = item.EstimateHours - scheduledItemHours
          const burnAmt = Math.min(remainingHrsInDay, remainingEstimateHrs)
          console.log(burnAmt)
          const endTime = currentTime.plus({hours: burnAmt})

          events.push(new ScheduleEvent(currentTime, endTime))

          scheduledItemHours = scheduledItemHours + burnAmt
          scheduledDayHours = scheduledDayHours + burnAmt
          currentTime = endTime
        }
        currentTime = currentTime.plus({days: 1}).startOf('day')
        scheduledDayHours = 0
      } while (scheduledItemHours < item.EstimateHours && this._scheduleInterval.contains(currentTime))
      if (scheduledItemHours < item.EstimateHours) {
        throw new Error("Work will overflow the schedule's date range.")
      }
    })

    this._events = events
  }

  get Events() {
    return this._events
  }

  public getAvailabilityForDate(date: DateTime): Duration {
    if (!this._scheduleInterval.contains(date)) {
      throw new Error('Date outside of schedule range')
    }
    if (this._nonWorkingDaysOfWeek?.includes(date.weekday)) {
      return Duration.fromMillis(0)
    }
    if (this._holidays?.find(holiday => holiday.toISODate() == date.toISODate())) {
      return Duration.fromMillis(0)
    }
    return this._workingHoursPerDay
  }

  public getAvailabilityForDays(range: Interval): AvailabilityForDay[] {
    return range.splitBy({ days: 1 }).map(day => {
      return {
        capacityHours: this.getAvailabilityForDate(day.start).hours,
        date: day.start
      }
    })
  }
}