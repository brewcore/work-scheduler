/*
This file is part of the Caplan software tool.

Copyright (C) 2021 Tyler Golden

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import WorkItem from '../src/lib/WorkItem'
import Schedule, { ScheduleConfig } from '../src/lib/Schedule'
import { DateTime, Duration, Interval } from 'luxon'
import { expect } from 'chai'

const item1 = new WorkItem('project 1', 'UID-1', 4)

const work = [
  item1,
  new WorkItem('project 1', 'UID-2', 8)
]

describe('Scheduling tests', () => {
  it('can have empty schedule', () => {
    const start = DateTime.fromISO('2022-01-04T00:00:00Z')
    const end = DateTime.fromISO('2022-01-04T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 8})
    })
    expect(schedule.Events).to.have.length(0)
  })

  it('calculates availability for one full day', () => {
    const start = DateTime.fromISO('2022-01-04T00:00:00Z')
    const end = DateTime.fromISO('2022-01-04T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5})
    })
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-04')).hours).to.eq(7.5)
  })

  it('throws if trying to get availability outside calendar range', () => {
    const start = DateTime.fromISO('2022-01-04T00:00:00Z')
    const end = DateTime.fromISO('2022-01-04T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5})
    })
    expect(() => schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-06'))).to.throw('Date outside of schedule range')
  })

  it('calculates availability for multiple days', () => {
    const start = DateTime.fromISO('2022-01-13T00:00:00Z')
    const end = DateTime.fromISO('2022-01-14T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5})
    })
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-13')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-14')).hours).to.eq(7.5)
  })

  it('calculates availability while ignoring non-working days', () => {
    const start = DateTime.fromISO('2022-01-13T00:00:00Z')
    const end = DateTime.fromISO('2022-01-17T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5}),
      nonWorkingDaysOfWeek: [6, 7]
    })
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-13')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-14')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-15')).hours).to.eq(0)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-16')).hours).to.eq(0)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-17')).hours).to.eq(7.5)
  })

  it('treats all days as workdays if nonWorkingDaysOfWeek not set', () => {
    const start = DateTime.fromISO('2022-01-13T00:00:00Z')
    const end = DateTime.fromISO('2022-01-17T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5})
    })
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-13')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-14')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-15')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-16')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-17')).hours).to.eq(7.5)
  })

  it('calculates availability while ignoring company holidays', () => {
    const start = DateTime.fromISO('2022-01-16T00:00:00Z')
    const end = DateTime.fromISO('2022-01-22T23:59:59.999Z')
    const schedule = new Schedule([], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 7.5}),
      nonWorkingDaysOfWeek: [6, 7],
      holidays: [DateTime.fromISO('2022-01-17')]
    })
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-16')).hours).to.eq(0)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-17')).hours).to.eq(0)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-18')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-19')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-20')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-21')).hours).to.eq(7.5)
    expect(schedule.getAvailabilityForDate(DateTime.fromISO('2022-01-22')).hours).to.eq(0)
  })

  it('puts single item immediately on the schedule', () => {
    const start = DateTime.fromISO('2022-01-01T00:00:00Z')
    const end = DateTime.fromISO('2022-01-31T23:59:59.999Z')
    const schedule = new Schedule([item1], {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 8})
    })
    expect(schedule.Events).to.have.length(1)
    expect(schedule.Events[0].Start.toUTC().toISO()).to.eq('2022-01-01T00:00:00.000Z')
    expect(schedule.Events[0].End.toUTC().toISO()).to.eq('2022-01-01T04:00:00.000Z')
  })

  it('schedules an item to start one day and end another', () => {
    const start = DateTime.fromISO('2022-01-01T00:00:00Z')
    const end = DateTime.fromISO('2022-01-31T23:59:59.999Z')
    const schedule = new Schedule(work, {
      start,
      end,
      workingHoursPerDay: Duration.fromObject({hours: 8})
    })
    expect(schedule.Events).to.have.length(2)
    expect(schedule.Events[0].Start.toUTC().toISO()).to.eq('2022-01-01T00:00:00.000Z')
    expect(schedule.Events[0].End.toUTC().toISO()).to.eq('2022-01-01T04:00:00.000Z')
    expect(schedule.Events[1].Start.toUTC().toISO()).to.eq('2022-01-01T04:00:00.000Z')
    expect(schedule.Events[1].End.toUTC().toISO()).to.eq('2022-01-02T04:00:00.000Z')
  })
})
